## spark-analytics

This folder contains information for deploying and running analytics job.

## Folder Structure

- ansible/: spark oshinko cluster deployment
- configs/openshift: configuration file for bc, is, and job
- experimental/: store any analytics sccripts that are still in progress
- machine-learning/: store all machine learning algorithms in use
- ops-pipeline/: Jenkinsfile to kick off job